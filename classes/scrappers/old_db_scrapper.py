from classes import helpers
from classes.scrappers.db_scrapper import DBScrapper
from classes.json_data.json_collection import JsonCollection


class OldDbScrapper(DBScrapper):

    DB_PICTURE_KEY = 'Screenshot'

    def __init__(self, host, port, database, username, password, storage_path):
        DBScrapper.__init__(self, host, port, database, username, password, storage_path)
        self.parsed_tables = [
            'Estate', 'Flats', 'Lands', 'OKS'
        ]

    def scrap(self):
        cur = self.connector.cursor()
        analogs = JsonCollection('analogs')

        for table in self.parsed_tables:
            rows = self.get_table_records(cur, "\"" + table + "\"")
            columns = self.get_table_colums(cur)
            for row in rows:
                pic_path = None
                row = helpers.create_db_record_dictionary_from_tuple(columns, row)
                if self.DB_PICTURE_KEY in row:
                    if row[self.DB_PICTURE_KEY] is not None:
                        pic_path = self.files_storage_path + row['ID_Analog'] + ".png"
                        helpers.store_image(pic_path, row[self.DB_PICTURE_KEY])
                    del row[self.DB_PICTURE_KEY]
                row['Picture_path'] = pic_path
                row['Type'] = table
                analogs.add_element(row)

        self.connector.close()

        analogs.serialize(self.storage_path, 'analogs')
