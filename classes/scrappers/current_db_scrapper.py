from classes import helpers
from classes.scrappers.db_scrapper import DBScrapper
from classes.json_data.json_collection import JsonCollection
from distutils.dir_util import copy_tree


def commercial_map_func(x: dict, json):
    return x.update({
        'analog_input_id': JsonCollection.where(json, 'id', x['analog_input_id']),
        'quality_finish_id': JsonCollection.where(json, 'id', x['quality_finish_id']),
        'wall_material_id': JsonCollection.where(json, 'id', x['wall_material_id'])
    })


def estate_map_func(x: dict, json):
    return x.update({
        'access_road_id': JsonCollection.where(json, 'id', x['access_road_id']),
        'quality_finish_id': JsonCollection.where(json, 'id', x['quality_finish_id']),
        'wall_material_id': JsonCollection.where(json, 'id', x['wall_material_id'])
    })


def flat_map_func(x: dict, json):
    return x.update({
        'bathroom_type_id': JsonCollection.where(json, 'id', x['bathroom_type_id']),
        'summer_option_id': JsonCollection.where(json, 'id', x['summer_option_id']),
        'quality_finish_id': JsonCollection.where(json, 'id', x['quality_finish_id']),
        'wall_material_id': JsonCollection.where(json, 'id', x['wall_material_id']),
    })


def parcel_map_func(x: dict, json):
    return x.update({
        'access_road_id': JsonCollection.where(json, 'id', x['access_road_id'])
    })


def get_sorted_comments_with_emails(path: list, comments_json: 'JsonCollection', users_json: 'JsonCollection'):
    comments = []
    for comment_id in path:
        new_comment = JsonCollection.where(comments_json, 'id', comment_id)
        if new_comment is not None:
            user = JsonCollection.where(users_json, 'id', new_comment['user_id'])
            if user is not None:
                new_comment['email'] = user['email']
            comments.append(new_comment)
    return comments


class CurrentDbScrapper(DBScrapper):

    def __init__(self, host, port, database, username, password, storage_path, analogs_date, path_to_file):
        DBScrapper.__init__(self, host, port, database, username, password, storage_path)
        self.schemas = [
            'analogs', 'documents'
        ]
        self.analogs_date = analogs_date
        self.path_to_files = path_to_file

    def scrap(self):
        cur = self.connector.cursor()

        #
        # Employee positions
        #

        employees_json = self.create_json_by_default_method(cur, 'employees', 'public.employee_positions')
        employees_json.serialize(self.storage_path, 'employee_positions')

        #
        # Factors
        #

        factors_json = self.create_json_by_default_method(cur, 'factors', 'public.factors')
        quality_factors_attributes_json = self.create_json_by_default_method(cur, 'quality_factor_attribute',
                                                                             'public.quality_factor_attributes')
        factors = factors_json.get_items()
        for factor in factors:
            if factor['type'] == 'quality':
                attrs = JsonCollection.get_all_where(quality_factors_attributes_json, 'factor_id', factor['id'])
                if len(attrs) > 0:
                    factor['attrs'] = [attr['value'] for attr in attrs]
        factors_json.set_items(factors)
        factors_json.serialize(self.storage_path, 'factors')

        #
        # Users
        #

        users_json = JsonCollection('users')
        users_records = self.get_table_records(cur, 'public.users')
        users_columns = self.get_table_colums(cur)
        for users_record in users_records:
            record = helpers.create_db_record_dictionary_from_tuple(users_columns, users_record)
            record['employee_position'] = JsonCollection.where(employees_json, 'id', record['employee_position_id'])
            users_json.add_element(record)

        users_json.serialize(self.storage_path, 'users')

        #
        # # Analogs
        #

        analogs_json = JsonCollection('analogs')
        analogs = self.get_table_records(cur, 'analogs.analogs', "updated_at > '" + self.analogs_date + "'")
        analogs_columns = self.get_table_colums(cur)

        segmentation_codes_json = self.create_json_by_default_method(cur, 'segmentation_code', 'segmentation.codes',
                                                                     self.get_external_table_unique_records_ids_query(
                                                                         analogs, [4, -1]))


        analogs_ids = self.get_external_table_unique_records_ids_query(analogs, [0], 'analog_id')

        analogs_attached_files_json = self.create_json_by_default_method(cur, 'analog_attached_files',
                                                                         'analogs.analog_attached_files', analogs_ids)
        analogs_commercials_json = self.create_json_by_default_method(cur, 'analog_commercial',
                                                                      'analogs.analog_commercials', analogs_ids).map(
            commercial_map_func, quality_factors_attributes_json)

        analogs_estates_json = self.create_json_by_default_method(cur, 'analog_estate', 'analogs.analog_estates',
                                                                  analogs_ids).map(estate_map_func,
                                                                                   quality_factors_attributes_json)
        analogs_flats_json = self.create_json_by_default_method(cur, 'analog_flat', 'analogs.analog_flats',
                                                                analogs_ids).map(flat_map_func,
                                                                                 quality_factors_attributes_json)
        analogs_parcels_json = self.create_json_by_default_method(cur, 'analog_parcel', 'analogs.analog_parcels',
                                                                  analogs_ids).map(parcel_map_func,
                                                                                   quality_factors_attributes_json)
        analogs_comments_json = self.create_json_by_default_method(cur, 'comments', 'analogs.analog_comments')

        analogs_factors_values = self.create_json_by_default_method(cur, 'analogs_factors_values',
                                                                    'analogs.analog_factor_values')

        for analog in analogs:
            new_analog_json_item = helpers.create_db_record_dictionary_from_tuple(analogs_columns, analog)
            if new_analog_json_item['permitted_use_land_id'] is not None:
                new_analog_json_item['permitted_use_land_id'] = JsonCollection.where(segmentation_codes_json, 'id',
                                                                                     new_analog_json_item[
                                                                                         'permitted_use_land_id'])

            if new_analog_json_item['permitted_use_cc_id'] is not None:
                new_analog_json_item['permitted_use_cc_id'] = JsonCollection.where(segmentation_codes_json, 'id',
                                                                                   new_analog_json_item[
                                                                                       'permitted_use_land_id'])

            if JsonCollection.where(analogs_commercials_json, 'analog_id', new_analog_json_item['id']):
                new_analog_json_item['analog_commercial'] = JsonCollection.where(analogs_commercials_json, 'analog_id',
                                                                                 new_analog_json_item['id'])

            if JsonCollection.where(analogs_estates_json, 'analog_id', new_analog_json_item['id']):
                new_analog_json_item['analog_estate'] = JsonCollection.where(analogs_estates_json, 'analog_id',
                                                                             new_analog_json_item['id'])

            if JsonCollection.where(analogs_flats_json, 'analog_id', new_analog_json_item['id']):
                new_analog_json_item['analog_flat'] = JsonCollection.where(analogs_flats_json, 'analog_id',
                                                                           new_analog_json_item['id'])

            if JsonCollection.where(analogs_parcels_json, 'analog_id', new_analog_json_item['id']):
                new_analog_json_item['analog_parcel'] = JsonCollection.where(analogs_parcels_json, 'analog_id',
                                                                             new_analog_json_item['id'])
            if JsonCollection.where(analogs_factors_values, 'analog_id', new_analog_json_item['id']):
                new_analog_json_item['factor_value'] = JsonCollection.where(analogs_factors_values,
                                                                            'analog_id', new_analog_json_item['id'])

            analog_comments_json = JsonCollection.get_all_where(analogs_comments_json, 'analog_id',
                                                                new_analog_json_item['id'])
            if len(analog_comments_json) > 0:
                up_path = CurrentDbScrapper.traverse_comments_up(analog_comments_json, analog_comments_json[0], [])
                down_path = CurrentDbScrapper.traverse_comments_down(analog_comments_json, analog_comments_json[0], [])
                new_analog_json_item['comments'] = get_sorted_comments_with_emails(
                    CurrentDbScrapper.full_traverse_path(up_path, down_path), analogs_comments_json, users_json)

            analog_attached_files = JsonCollection.get_all_where(analogs_attached_files_json, 'analog_id',
                                                                 new_analog_json_item['id'])
            if len(analog_attached_files) > 0:
                new_analog_json_item['attached'] = analog_attached_files

            analogs_json.add_element(new_analog_json_item)
        analogs_json.serialize(self.storage_path, 'analogs')
        del analogs_json
        #
        # # Documents
        #

        documents_categories_json = self.create_json_by_default_method(cur, 'documents_categories',
                                                                       'documents.document_categories')
        documents_categories_json.serialize(self.storage_path, 'documents_categories')

        documents_statuses_json = self.create_json_by_default_method(cur, 'document_statuses',
                                                                     'documents.document_statuses')
        documents_statuses_json.serialize(self.storage_path, 'documents_statuses')

        documents_types_json = self.create_json_by_default_method(cur, 'documents_types',
                                                                  'documents.document_types')
        documents_template_types_json = self.create_json_by_default_method(cur, 'documents_templates_types',
                                                                           'documents.document_template_types')

        documents_attached_files_json = self.create_json_by_default_method(cur, 'documents_attached_files_json',
                                                                           'documents.document_attached_files')

        documents_comments_json = self.create_json_by_default_method(cur, 'documents_comments',
                                                                     'documents.document_comments')
        documents_contractor_documents_json = self.create_json_by_default_method(cur, 'documents_contractor_documents',
                                                                                 'documents.document_contractor_document')

        documents_contractors_json = self.create_json_by_default_method(cur, 'documents_contractors',
                                                                        'documents.document_contractors')

        documents_related_json = self.create_json_by_default_method(cur, 'document_relate',
                                                                    'documents.document_related')

        documents_responses_json = self.create_json_by_default_method(cur, 'document_response',
                                                                      'documents.document_responses')

        documents_evaluated_object_json = self.create_json_by_default_method(cur, 'document_evaluated_object_ids',
                                                                             'documents.evaluated_object_document')

        evaluated_objects_json = self.create_json_by_default_method(cur, 'evaluated_object',
                                                                    'public.evaluated_objects',
                                                                    self.get_external_table_unique_records_ids_query(
                                                                        documents_evaluated_object_json.get_items(),
                                                                        ['evaluated_object_id']))

        documents_json = JsonCollection('documents')
        documents = self.get_table_records(cur, 'documents.documents')
        documents_column = self.get_table_colums(cur)
        documents_ids = self.format_where_in_query(self.get_table_column_records_as_string(documents), 'document_id')

        documents_related_tables = {
            'documents.evaluated_object_document': {
                'ids': documents_ids,
                'key': 'document_id'
            },
        }
        documents_related_tables_data = self.related_tables_data(cur, documents_related_tables)

        eval_objs_key = 'public.evaluated_objects'
        documents_related_tables_data[eval_objs_key] = self.related_tables_data(cur, {
            eval_objs_key: {
                'ids': self.format_where_in_query(self.get_table_column_records_as_string(set(
                    [str(value[0]['evaluated_object_id']) for key, value in
                     documents_related_tables_data['documents.evaluated_object_document'].items()]))),
                'key': 'id'
            }
        })[eval_objs_key]

        for document in documents:
            new_document_json_item = helpers.create_db_record_dictionary_from_tuple(documents_column, document)
            if new_document_json_item['creator_id'] is not None:
                new_document_json_item['creator_id'] = \
                    JsonCollection.where(users_json, 'id', new_document_json_item['creator_id'])['email']
            if new_document_json_item['executor_id'] is not None:
                new_document_json_item['executor_id'] = \
                    JsonCollection.where(users_json, 'id', new_document_json_item['executor_id'])['email']
            if new_document_json_item['document_type_id'] is not None:
                new_document_json_item['document_type_id'] = JsonCollection.where(documents_types_json, 'id',
                                                                                  new_document_json_item[
                                                                                      'document_type_id'])
            if new_document_json_item['document_status_id'] is not None:
                new_document_json_item['document_status_id'] = JsonCollection.where(documents_statuses_json, 'id',
                                                                                    new_document_json_item[
                                                                                        'document_status_id'])
            if new_document_json_item['document_category_id'] is not None:
                new_document_json_item['document_category_id'] = JsonCollection.where(documents_categories_json, 'id',
                                                                                      new_document_json_item[
                                                                                          'document_category_id'])
            if new_document_json_item['document_template_type_id'] is not None:
                new_document_json_item['document_template_type_id'] = JsonCollection.where(
                    documents_template_types_json, 'id',
                    new_document_json_item['document_template_type_id'])
            if new_document_json_item['document_type_id'] is not None:
                new_document_json_item['document_type'] = JsonCollection.where(documents_types_json, 'id',
                                                                               new_document_json_item[
                                                                                   'document_type_id'])
            document_attached_files = JsonCollection.get_all_where(documents_attached_files_json, 'document_id',
                                                                   new_document_json_item['id'])
            if len(document_attached_files) > 0:
                new_document_json_item['attached'] = JsonCollection.get_all_where(documents_attached_files_json,
                                                                                  'document_id',
                                                                                  new_document_json_item['id'])

            document_comments_json = JsonCollection.get_all_where(documents_comments_json,
                                                                  'document_id',
                                                                  new_document_json_item['id'])
            if len(document_comments_json) > 0:
                up_path = CurrentDbScrapper.traverse_comments_up(document_comments_json, document_comments_json[0], [])
                down_path = CurrentDbScrapper.traverse_comments_down(document_comments_json, document_comments_json[0],
                                                                     [])
                new_document_json_item['comments'] = get_sorted_comments_with_emails(
                    CurrentDbScrapper.full_traverse_path(up_path, down_path), documents_comments_json, users_json)

            if JsonCollection.where(documents_contractor_documents_json, 'document_id', new_document_json_item['id']):
                related_table_record = JsonCollection.where(documents_contractor_documents_json,
                                                            'document_id',
                                                            new_document_json_item['id'])
                new_document_json_item['contractor'] = JsonCollection.where(documents_contractors_json,
                                                                            'id',
                                                                            related_table_record[
                                                                                'document_contractor_id'])

            related_document = JsonCollection.where(documents_related_json, 'document_id', new_document_json_item['id'])
            if related_document is not None:
                new_document_json_item['related'] = related_document

            response_document = JsonCollection.where(documents_responses_json, 'document_id',
                                                     new_document_json_item['id'])
            if response_document:
                new_document_json_item['response'] = response_document

            doc_eval_obj = JsonCollection.where(documents_evaluated_object_json, 'document_id', new_document_json_item['id'])
            if doc_eval_obj:
                new_document_json_item['evaluated_object'] = JsonCollection.where(evaluated_objects_json,
                                                                                  'id',
                                                                                  doc_eval_obj['evaluated_object_id'])['cadastral_num']

            documents_json.add_element(new_document_json_item)

        documents_json.serialize(self.storage_path, 'documents')
        #
        # # Files
        #

        # Analogs

        copy_tree(self.path_to_files + "/analog", self.files_storage_path + "/analogs")

        # Documents

        copy_tree(self.path_to_files + "/documents", self.files_storage_path + "/documents")

    def related_tables_data(self, cursor, tables):
        res = {}
        for table, data in tables.items():
            res[table] = {}
            rows = self.get_table_records(cursor, table, data['ids'])
            columns = self.get_table_colums(cursor)
            for row in rows:
                row_with_columns = helpers.create_db_record_dictionary_from_tuple(columns, row)
                if row_with_columns[data['key']] not in res[table]:
                    res[table][row_with_columns[data['key']]] = []
                    res[table][row_with_columns[data['key']]].append(row_with_columns)
                else:
                    res[table][row_with_columns[data['key']]].append(row_with_columns)
        return res

    def get_external_table_unique_records_ids_query(self, table_records, ids, prefix='id'):
        records = []
        [records.extend([item[index] for index in ids]) for item in table_records]
        unique_records_str = ', '.join([str(x) for x in set(records) if x is not None])
        return self.format_where_in_query(unique_records_str, prefix)

    def get_table_column_records_as_string(self, records, column_index=0):
        return ', '.join(str(element) for element in [item[column_index] for item in records])

    def format_where_in_query(self, data, prefix='id'):
        return "{prefix} in ({ids_list})".format(prefix=prefix, ids_list=data)

    @staticmethod
    def traverse_comments_up(comments: 'JsonCollection', current_comment, path: list):
        path.append(current_comment['id'])
        if current_comment['comment_id'] is None:
            return path
        else:
            next_traverse_comment = JsonCollection.where(comments, 'id', current_comment['comment_id'])
            return CurrentDbScrapper.traverse_comments_up(comments, next_traverse_comment, path) \
                if next_traverse_comment is not None else path

    @staticmethod
    def traverse_comments_down(comments: 'JsonCollection', current_comment, path: list):
        path.append(current_comment['id'])
        if current_comment['comment_id'] is None:
            return path
        else:
            next_traverse_comment = JsonCollection.where(comments, 'comment_id', current_comment['id'])
            return CurrentDbScrapper.traverse_comments_up(comments, next_traverse_comment, path) \
                if next_traverse_comment is not None else path

    @staticmethod
    def full_traverse_path(up_path: list, down_path: list):
        up = up_path[::-1]
        down = down_path[1:] if len(down_path) > 1 else []
        res = up + down
        return res

    def create_json_by_default_method(self, cursor, object_type, table_name, get_records_condition=None):
        json = JsonCollection(object_type)
        records = self.get_table_records(cursor=cursor, table=table_name, condition=get_records_condition)
        columns = self.get_table_colums(cursor)
        [json.add_element(helpers.create_db_record_dictionary_from_tuple(columns, record)) for record in records]
        return json
