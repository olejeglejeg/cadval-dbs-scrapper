from abc import abstractmethod
from classes import db_helper, helpers


class DBScrapper:

    def __init__(self, host, port, database, username, password, storage_path):
        self.connector = db_helper.get_connector(
            host=host,
            port=port,
            database=database,
            username=username,
            password=password
        )
        self.storage_path = storage_path
        self.files_storage_path = helpers.get_files_storage_path(storage_path)

    @abstractmethod
    def scrap(self):
        pass

    def select_query(self, cursor, table,  condition=None):
        query = "SELECT * FROM " + table
        if condition is not None:
            query += " WHERE " + condition
        query += ";"
        cursor.execute(query)
        return cursor

    def obtain_query_data(self, cursor):
        return cursor.fetchall()

    def get_table_records(self, cursor, table, condition=None):
        return self.obtain_query_data(self.select_query(cursor, table, condition))

    def get_table_colums(self, cursor):
        return [desc[0] for desc in cursor.description]
