import psycopg2


def get_connector(host, port, database, username, password):
    try:
        connection = psycopg2.connect(
            user=username,
            password=password,
            host=host,
            port=port,
            database=database
        )
        cursor = connection.cursor()
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        print('You are connected to - ', record,)
    except (Exception, psycopg2.Error) as error:
        print ("Error while connecting to PostgreSQL", error)
        connection = "failed"

    return connection


def get_schema_tables_list(cursor, schema_name):
    cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema='" + schema_name + "';")
    data = cursor.fetchall()
    unwrapper = lambda item: item[0]
    return map(unwrapper, data)
