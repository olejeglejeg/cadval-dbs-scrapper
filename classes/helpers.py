import os


def create_directory_if_not_exists(directory):
    if not os.path.exists(directory):
        print("Create directory", directory)
        os.makedirs(directory)


def store_image(path, binary_data):
    new_file = open(path, "wb")
    new_file.write(bytearray(binary_data))


def get_files_storage_path(path):
    return path + "files/" if path.endswith('/') else path + "/files/"


def create_db_record_dictionary_from_tuple(columns, row_tuple):
    return dict(zip(columns, row_tuple))
