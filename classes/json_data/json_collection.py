import json
import sys
from importlib import reload


class JsonCollection:

    def __init__(self, object_type: str):
        self.object_type = object_type
        self.count = 0
        self.items = []

    def get_objects_types(self):
        return self.object_type

    def get_count(self):
        return self.count

    def set_count(self, value: int):
        self.count = value

    def get_items(self):
        return self.items

    def set_items(self, items):
        self.items = items

    def add_element(self, element: dict):
        self.items.append(element)

    def remove_element(self, key: str, element_id):
        items = self.get_items()
        for item in items:
            if item[key] == element_id:
                del item
                break
        self.set_items(items)

    @staticmethod
    def where(json_collection: 'JsonCollection', key: str, value):
        try:
            res = next(item for item in json_collection.get_items() if item[key] == value)
        except:
            res = None
        return res

    @staticmethod
    def get_all_where(json_collection: 'JsonCollection', key: str, value):
        res = []
        for item in json_collection.get_items():
            if item[key] == value:
                res.append(item)
        return res

    def format_object(self):
        return {
            'type': self.object_type,
            'count': self.count,
            'items': self.items
        }

    def serialize(self, path: str, file_name: str, extension='.json'):
        self.set_count(len(self.get_items()))

        reload(sys)
        file_path = path + file_name + extension

        with open(file_path, 'w') as f:
            f.write(json.dumps(self.format_object(), ensure_ascii=False, default=str))

    def map(self, func, replace_from):
        items = self.get_items()
        for item in items:
            item = func(item, json=replace_from)

        self.set_items(items)
        return self
