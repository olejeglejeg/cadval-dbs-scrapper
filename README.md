# Cadval DBs scrapper
## How to run:
1. Run ```
            pip install --trusted-host pypi.python.org -r requirements.txt
       ```
2. Go to .env and specify key variables
3. Run ```
            python3 main.py
       ```
4. All parsed data will be placed into 'storage' folder
