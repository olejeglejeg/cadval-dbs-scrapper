from classes import helpers
from classes.scrappers.old_db_scrapper import OldDbScrapper
from classes.scrappers.current_db_scrapper import CurrentDbScrapper
from dotenv import load as load_dotenv
import os

old_storage_path = "storage/old/"
old_files_storage_path = "storage/old/files/"
current_storage_path = "storage/current/"
current_files_storage_path = "storage/current/files/"


def main():
    dbs = [
        'old',
        'current',
    ]

    for db_sign in dbs:
        if db_sign == 'old':
            helpers.create_directory_if_not_exists(old_files_storage_path)
            old_db_scrapper = OldDbScrapper(env('OLD_DB_HOST'),
                                            env('OLD_DB_PORT'),
                                            env('OLD_DB_DATABASE'),
                                            env('OLD_DB_USERNAME'),
                                            env('OLD_DB_PASSWORD'),
                                            old_storage_path)
            if is_scrapper_correct(old_db_scrapper):
                old_db_scrapper.scrap()
            del old_db_scrapper
        elif db_sign == 'current':
            helpers.create_directory_if_not_exists(current_files_storage_path)
            current_db_scrapper = CurrentDbScrapper(env('CURRENT_DB_HOST'),
                                                    env('CURRENT_DB_PORT'),
                                                    env('CURRENT_DB_DATABASE'),
                                                    env('CURRENT_DB_USERNAME'),
                                                    env('CURRENT_DB_PASSWORD'),
                                                    current_storage_path,
                                                    env('CURRENT_DB_ANALOGS_FROM_TIME'),
                                                    env('CURRENT_DB_PATH_TO_STORAGE'))
            if is_scrapper_correct(current_db_scrapper):
                current_db_scrapper.scrap()
            del current_db_scrapper
        else:
            print("For database \"" + db_sign + "\" call,", "nothing is done!")


def is_scrapper_correct(scrapper):
    res = True
    if scrapper.connector == "failed":
        res = False
    return res


def env(key):
    return os.getenv(key)


if __name__ == '__main__':
    load_dotenv('.env')
    main()
